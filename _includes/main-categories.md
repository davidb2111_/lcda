<div id="main-categories">
  <div class="category">
  <a href="{{ site.url | append: '/categories/sweaters.html' }}">
    {% assign image_path = 'pull-miss-manga' | prepend: 'assets/images/' | append: '/featured.jpg' %}
    {% responsive_image_block %}
      path: {{image_path}}
      class: 'object-fit_cover'
    {% endresponsive_image_block %}
    <div class=background><div class="text">
    {% case site.active_lang %}
    {% when 'fr' %}
    Pulls, tops et gilets
    {% when 'en' %}
    Sweaters, tops and jackets
    {% endcase %}
    </div></div>
  </a>
  </div>
  <div class="category">
  <a href="{{ site.url | append: '/categories/shawls.html' }}">
    {% assign image_path = 'summer-in-sevilla' | prepend: 'assets/images/' | append: '/featured.jpg' %}
    {% responsive_image_block %}
      path: {{image_path}}
      class: 'object-fit_cover'
    {% endresponsive_image_block %}
    <div class=background><div class="text">
    {% case site.active_lang %}
    {% when 'fr' %}
    Châles, écharpes et wraps
    {% when 'en' %}
    Shawls, scarfs and wraps
    {% endcase %}
    </div></div>
  </a>
  </div>
  <div class="category">
  <a href="{{ site.url | append: '/categories/accessories.html' }}">
    {% assign image_path = 'mitaines-dentelle' | prepend: 'assets/images/' | append: '/featured.jpg' %}
    {% responsive_image_block %}
      path: {{image_path}}
      class: 'object-fit_cover'
    {% endresponsive_image_block %}
    <div class=background><div class="text">
    {% case site.active_lang %}
    {% when 'fr' %}
    Maison et accessoires
    {% when 'en' %}
    Accessories
    {% endcase %}
    </div></div>
  </a>
  </div>
  <div class="category">
  <a href="{{ site.url | append: '/categories/free-patterns.html' }}">
    {% assign image_path = 'en-attendant-la-neige' | prepend: 'assets/images/' | append: '/featured.jpg' %}
    {% responsive_image_block %}
      path: {{image_path}}
      class: 'object-fit_cover'
    {% endresponsive_image_block %}
    <div class=background><div class="text">
    {% case site.active_lang %}
    {% when 'fr' %}
    Modèles gratuits
    {% when 'en' %}
    Free patterns
    {% endcase %}
    </div></div>
  </a>
  </div>
</div>
