<script type="text/javascript">
  {% case site.active_lang %}
  {% when 'fr' %}
  all_posts = '/all-{{ include.category }}.{{ site.active_lang }}.json';
  {% when 'en' %}
  all_posts = '/en/all-{{ include.category }}.{{ site.active_lang }}.json';
  {%  endcase %}
</script>
<div class="post-list">
  <div id="wrap_all">
    <div id="main" data-scroll-offset="0">
      <div class="post-list-left">
        {% assign posts = site.posts |where:"main_category",include.category %}
        {% for post in posts limit: 5 %}
          {% include post.html %}
        {% endfor %}
      </div>
    </div>
  </div>
</div>

<div class="infinite-spinner"></div>

