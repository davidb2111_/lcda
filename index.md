---
layout: home
title: Home
slug: home
paginate:
  permalink: /page/:num/
support:
  - jquery
---

<div class="post-list">
  <div id="wrap_all">
    <div id="main" data-scroll-offset="0">
    <section><h2>
      {% case site.active_lang %}
      {% when 'fr' %}
        Dernier article
      {% when 'en' %}
        Last post
      {% endcase %}
      </h2>
        {% include last-post.md %}
      </section>

      <section><h2>
      {% case site.active_lang %}
      {% when 'fr' %}
        Et aussi…
      {% when 'en' %}
        And also…
      {% endcase %}
      </h2>
        {% include main-categories.md %}
      </section>
    </div>
  </div>
</div>
