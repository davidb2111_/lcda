require 'cgi'
require 'pp'

module Jekyll
  class PageWithoutAFile < Page
    def read_yaml(*)
      @data ||= {}
    end
  end

  module PermalinkBuilder
    extend self

    def get_adjusted_permalink(resource, layout)
      layout_path = CGI.escape(layout)
      url = resource.url
      ext = File.extname(url)

      if url.include?(':layout')
        return url.gsub(/:layout/, layout_path)
      end

      if ext.empty?
        "#{url}/#{layout_path}/"
      else
        url.gsub(/\/$|#{ext}$/) { |url_end| "/#{layout_path}#{url_end}" }
      end
    end
  end

  class SummaryGenerator < Jekyll::Generator
    safe true
    priority :highest

    def generate(site)
      @site = site
      generator
    end

    private

    def generator
      @site.collections.each do |_, collection|
        # for each collection, create a summary of each posts
        collection.docs.each do |doc|
          @site.pages << create_layout_views(doc)
        end
      end
    end

    def create_layout_views(doc)
      Jekyll::PageWithoutAFile.new(@site, @site.source, "", "summary.html").tap do |new_page|
        new_page.data = doc.data.dup
        new_page.content = doc.content.dup
        new_page.data["layout"] = "summary"
        new_page.data["original"] = doc.permalink
        new_page.data["sitemap"] = false
        new_page.data["permalink"] = PermalinkBuilder.get_adjusted_permalink(doc, 'summary')
      end
    end

  end
end
