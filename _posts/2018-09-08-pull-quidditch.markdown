---
layout: post
lang: fr
title: "Pull de Quidditch Gryffondor Harry Potter"
date: 2018-09-08 18:33:00
categories: ['tricot', 'création']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "Un pull de quidditch Gryffondor avec explications gratuites"
asset_path: 'pull-quidditch'
license: 'cc-by-sa'
tags:
  - 'pull'
  - 'création'
  - 'tricot'
  - 'quidditch'
  - 'Annell'
  - 'Gryffondor'
  - 'HarryPotter'
  - 'gratuit'
support:
  - jquery
  - gallery
redirect_from: 
  - "/pull-quiddich/"
---

Bonjour à tous et toutes!

C'est la rentrée, et les aiguilles ont chauffé pendant les vacances: voici une réplique du pull de quidditch Gryffondor Harry Potter!  
Les enfants aiment vraiment cet univers, mais il faut avouer que les objets s'y rapportant ne sont pas donnés!  
L'idée était de faire un pull assez moulant, genre "pull chaussette".

Il est tricoté en côtes aux aiguilles 3.5mm, avec un fil acrylique "Rapido" de chez Annell, une première pour moi qui ne suis pas fan du 100% acrylique,
mais je dois avouer que ce fil m'a conquise: d'une douceur extrême et très très sympa à tricoter!

Aux couleurs de Gryffondor pour mon Astrid, mais facilement adaptable en fonction de votre maison.  

{% include gallery-layout.html gallery=site.data.galleries.pull-quidditch %}

Taille: 8 ans  
Aiguilles 3.5mm  
Echantillon: 10x10 = 26 mailles x 27 rangs  
5 pelotes de Rapido de chez Annell coloris rouge bordeaux  
2 pelotes de Rapido de chez Annell coloris jaune or  
1 écusson Harry Potter pas trop grand (chez Cinéreplicas sur Amazon)

## Dos

Monter 80 mailles aiguilles n°3.5 coloris rouge, tricoter 4cm en côtes 1/1.  
Tricoter ensuite en côtes 2/1.  
A 10.5cm de hauteur totale, changer pour le coloris jaune et tricoter 2 rangs.  
Reprendre le coloris rouge, puis à 25cm de hauteur totale, changer pour le coloris jaune et tricoter 22 rangs en jaune.

#### Emmanchures

Reprendre le coloris rouge, tricoter 2 rangs et rabattre de chaque côté 1x3m, 1x2m et 1x1m.  
Continuer sur les 68 mailles restantes.  
A 47cm de hauteur totale, rabattre pour l'encolure les 20 m centrales, puis 2x3m.  
A 49.5cm de hauteur totale, rabattre pour les épaules 2x4m et 2x5 mailles.

## Devant

Monter 80 mailles coloris rouge aiguilles 3.5mm, tricoter 4cm en côtes 1/1.  
Tricoter ensuite en côtes 2/1.  
A 4.5cm de hauteur totale, changer pour le coloris jaune et tricoter 2 rangs.  
Reprendre le coloris rouge.  
A 19 cm de hauteur totale, changer pour le coloris jaune et tricoter 22 rangs en jaune.

#### Emmanchures

Reprendre le coloris rouge, tricoter 2 rangs et rabattre de chaque côté 1x3m, 1x2m et 1x1m.  
Continuer sur les 68 mailles restantes.  
A 39cm de hauteur totale, rabattre pour l'encolure les 14 m centrales, puis 2x3m, 1x2m et 1,1m.  
A 43.5cm de hauteur totale, rabattre pour les épaules 2x4m et 2x5 mailles.

## Manches

Monter 42 mailles en rouge aiguilles 3.5mm.  
Tricoter 4cm en côtes 1/1, coloris rouge.  
Faire 2x1 augmentations de chaque côté tous les 20 rangs, et 1 augmentation 18 rangs plus haut en respectant les couleurs ainsi:  
2 rangs de rouge après les côtes 1/1, 2 rangs de jaune, reprendre en rouge jusqu'à 26.5cm de hauteur totale, 22 rangs de jaune, 2 rangs de rouge.  
Puis commencer les arrondis en rabattant de chaque côté:  
tous les 2 rangs 1x3m, 1x2m et 3x1m,  
tous les 4 rangs 4x1m,  
tous les 2 rangs 1x1m, 1x2m et 1x3m.

## Montage et col

Coudre les côtés, les épaules, les manches en prenant bien garde de faire correspondre les lignes.  
Relever les mailles de l'encolure avec le coloris jaune, tricoter en rond avec aiguilles circulaires 1 rang jaune (=2 rangs avec rang de relevage) et 5 rangs en coloris rouge.  
Rabattre les mailles.  
Coudre l'écusson sur le côté gauche du pull.

Et voilà, vous pouvez enfourcher votre balai et arborer fièrement les couleurs de votre maison et de votre équipe de quidditch!

A très bientôt et bon tricot!

Hélène
