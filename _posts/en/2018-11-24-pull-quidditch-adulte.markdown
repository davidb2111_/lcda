---
layout: post
lang: en
locale: en
title:  "Quidditch sweater Hufflepuff Harry Potter"
date:   2018-11-24 16:00:00
categories: ['tricot', 'création', 'favorite']
main_category: 'sweaters'
comments: true
meta_description: "A Harry Potter quidditch sweater, adult size, with Hufflepuff colours"
asset_path: 'pull-quidditch-adulte'
license: 'commercial'
favorite: 80
tags:
  - 'sweater'
  - 'creation'
  - 'knitting'
  - 'knit'
  - 'quidditch'
  - 'Zeeman'
  - 'Hufflepuff'
  - 'HarryPotter'
  - 'pattern'
support:
  - jquery
  - gallery
---

Here it is finally in knitting pattern, the Quidditch adult size sweater!  
In one size (corresponds to a S/M) thanks to the 2/1 rib points that allow it to expand!

Knitted here in Hufflepuff, but you just have to change the colors to proudly represent your house!

{% include gallery-layout.html gallery=site.data.galleries.pull-quidditch-adulte id_number=1 %}

Very easy and quick to knit: needles US 7, no stitches or complicated patterns, just ribs and 2 colours!

And you, are you Gryffindor, Hufflepuff, Ravenclaw or Slytherin?

This sweater, like the original model, is longer on the back than in front.  
If you don't like this effect, you knit the lower back like the front (contrasting strip at the same height as the front)!

This pattern can be purchased from [my Ravelry space](https://www.ravelry.com/patterns/library/quidditch-sweater-3).

Hélène
