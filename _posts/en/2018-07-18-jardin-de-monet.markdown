---
layout: post
lang: en
locale: en
title:  "Shawl Jardin de Monet"
date:   2018-07-18 19:00:00
categories: ['crochet']
main_category: 'shawls'
comments: true
meta_description: "A gradient crochet shawl with free pattern"
asset_path: 'jardin-de-monet'
license: 'cc-by-sa'
tags:
  - 'shawl'
  - 'crochet'
  - 'Freia'
  - 'Bluevelvet'
  - 'realization'
  - 'jardin de monet'
  - 'gradient'
  - 'summer'
support:
  - jquery
  - gallery
---

Good morning everyone!

Small holiday break for the creations, but the needles and the hook still work!
I give you an appointment at the beginning of the school year for many new creations, while waiting I present you my summer achievements!

Here is today a light shawl for summer evenings, with aquatic colors,  I love it!

{% include gallery-layout.html gallery=site.data.galleries.jardin-de-monet id_number=1 %}

Made with 3,5mm crochet with 2 balls of the beautiful [Merino Fingering Shawl Ball](https://freiafibers.com/blue-velvet.html) from Freiafibers.

You can find Sylvie Bangert's "Jardin de Monet" model, for free, [here](https://www.ravelry.com/patterns/library/jardin-de-monet).

See you soon for new realizations!
Have a good day and a good hook!

Hélène

