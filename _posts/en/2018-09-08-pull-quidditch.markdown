---
layout: post
lang: en
locale: en
title:  "Quidditch sweater Gryffondor from Harry Potter"
date:   2018-09-09 9:45:00
categories: ['tricot', 'création']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "A quidditch sweater Gryffondor with free pattern"
asset_path: 'pull-quidditch'
license: 'cc-by-sa'
tags:
  - 'sweater'
  - 'creation'
  - 'knit'
  - 'quidditch'
  - 'Annell'
  - 'Gryffondor'
  - 'HarryPotter'
  - 'free'
  - 'pattern'
support:
  - jquery
  - gallery
redirect_from: 
  - "/pull-quiddich/" 
---

Hi Everyone!

Kids are back to school, but the needles heated up during the holidays: here is a replica of the quidditch Gryffindor Harry Potter sweater!  
The idea was to make a tight sweater, like a "sock sweater".

It is knitted in 4 needles (3.5mm), with "Rapido", an acrylic yarn from Annell, an extremly soft yarn and very very very nice to knit!

In the Gryffindor colours for my daughter, but easily adaptable to suit your home.  

{% include gallery-layout.html gallery=site.data.galleries.pull-quidditch %}

Size: 8 years  
Needles 4 (3.5mm)  
Gauge: 10x10 = 26 mailles x 27 rangs  
5 skein of Rapido by Annell color red *bordeaux*  
2 skein of Rapido by Annell color gold  
1 patch Harry Potter (Cinéreplicas on Amazon)

## Back

CO 80 stitches with 4 neddles in red, \*K1, P1\* on 4cm.  
Then, \*K2, P1\* on the right side, knit and purl stitches as they appears on wrong side.  
At 10.5cm of total height, change for the gold color and knit/purl 2 rows.  
Take back the red color, then at 25cm of total height, change for gold color and make 22 rows in gold.

#### Armhole

Take back red color, make 2 rows and bind off on each side 1x3st, 1x2st and 1x1st.  
Continue on the 68 stitches.  
At 47cm of total height, bind off for the neck the 20 central stitches, then 2x3st.  
At 49.5cm of total height, bind off for shoulders 2x4st and 2x5st.  

## Front

CO 80 stitches with 4 neddles in red, \*K1, P1\* on 4cm.  
Then, \*K2, P1\* on the right side, knit and purl stitches as they appears on wrong side.  
At 4.5cm of total height, change for the gold color and knit/purl 2 rows.  
Take back the red color, then at 19cm of total height, change for gold color and make 22 rows in gold.

#### Armhole

Take back red color, make 2 rows and bind off on each side 1x3st, 1x2st and 1x1st.  
Continue on the 68 stitches.  
At 39cm of total height, bind off for the neck the 14 central stitches, then 2x3st, 1x2st et 1x1st.  
At 43.5cm of total height, bind off for shoulders 2x4st and 2x5st.


## Sleeves (x2)

CO 42 stitches with 4 neddles in red, \*K1, P1\* on 4cm.  
Increase 2x1st both side each 20 rows, and increase 1st on both sides 18 rows higher, and respect colors like that:  
2 rows red after ribs 1/1, 2 rows gold, take back red color until 26.5cm of total height, 22 rows gold color, 2 rows red color.  
Then create the rounds and bind off on both side:  
every 2 rows 1x3st, 1x2st et 3x1st,  
every 4 rows 4x1st,  
every 2 rows 1x1st, 1x2st et 1x3st.

## Assembly and neck

Sew the sides of the sweater, the shoulders, the sleeves making sure to match the lines.  
Pick up stitches around the neck with gold color, \*K1, P1\* for 1 row with gold color (=2 rows with pick-up row) and 5 rows in red color.  
Fasten off.  
Sew the patch on the left side of the sweater.

Now you can get on your broom and proudly wear the colours of your home and your quidditch team!

See you soon and good knitting!

Hélène
