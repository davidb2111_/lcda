---
layout: post
lang: en
title:  "Wednesday Sweater"
date:   2023-01-28 17:00:00
categories: ['tricot', 'creation']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "A knitted sweater with black and white squares from the series Wednesday"
asset_path: 'pull-wednesday'
license: 'cc-by-sa'
tags:
  - 'sweater'
  - 'knit'
  - -knitting'
  - 'sweat'
  - 'creation'
  - 'wednesday'
support:
  - jquery
  - gallery
---
Hello to everyone!

I'm back on the website after a long time.
I watched like many of you the Tim Burton series "**Wednesday**" on Netflix, and my daughter asked me to make her the "square" sweater worn by Wednesday.

So I created a sweater pattern (one of the many you can find on the net at this time!) that I am pleased to offer you today!

{% include gallery-layout.html gallery=site.data.galleries.pull-wednesday %}

I think it's pretty close to the original, I'm quite satisfied with it (for once I like one of my creations!)
So don't hesitate to click on the attached pdf below, and enjoy your knitting!

{% include download-layout.html pdflink="/assets/pdf/wednesday-sweater-lcda.pdf" name="Wednesday-Sweater.pdf" %}


