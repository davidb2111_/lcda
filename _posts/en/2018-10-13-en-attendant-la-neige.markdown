---
layout: post
lang: en
locale: en
title:  "Waiting for the Snow"
date:   2018-10-13 13:14:00
categories: ['tricot', 'création']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "A very soft sweater with free pattern in all size"
asset_path: 'en-attendant-la-neige'
license: 'cc-by-sa'
tags:
  - 'sweater'
  - 'knit'
  - 'soft'
  - 'hygge'
  - 'loose'
  - 'creation'
  - 'free'
  - 'pattern'
  - 'Zeeman'
  - 'snow'
support:
  - jquery
  - gallery
---

Hello to all of you!

We continue with creations, with today an ultra simple model to realize: a loose sweater that I named "Waiting for the snow": its softness and whiteness make me think of a warm afternoon by the fire to watch the snow fall outside!  
Another very, very fast model to make with needles number 7, with 3 balls of Julia yarn from Zeeman for the size 8 years: you can knit in less than 10 hours and for less than 10 euros!  
And it will do wonders for your outings with its fancy stitch, alternating garter stitch and openwork river stitch!  
I offer you here the explanations to realize it in all sizes, child 8 years old and adult, without any headache!  
So, beginner or advanced, get started!

{% include gallery-layout.html gallery=site.data.galleries.en-attendant-la-neige id_number=1%}

You will need:
* Julia yarn from Zeeman, 3 balls in 8 years, 6 balls for the other sizes (to be confirmed)
* N°7 needles

Fancy stitch:  
10 rows of garter stitch (= a "garter block"), 1 row of river stitch (wrap the thread twice around the needle, release one of the 2 threads on the return, tuto to findi [here] (https://www.garnstudio.com/video.php?id=192&lang=en)

## Front and back

You will knit 2 rectangles for the front and the back.
* Size 38-40: width 56cm, height 62cm approx.
* Size 42-44: width 61cm, same height
* Size 46-48: width 64cm, same height

Here, for a size 8 years I made 2 rectangles of 40cm large by 42cm high.

{% include gallery-layout.html gallery=site.data.galleries.en-attendant-la-neige-carre id_number=2%}

So cast on your stitches (50 in size 8 years) (70/76/80 stitches) and knit in fancy stitch up to the desired height (87 rows in 8 years).  
Be careful to fast off after the 10th row of garter stitch.  
Make a second identical piece.  
Sew the sides leaving a wide opening for the armhole (about 16 cm for the armhole in 8 years, count 20 cm for a 38, 21cm for a 40-42 and 22cm for a 44-46).  
Sew the shoulders together, leaving an opening for the head, a little wider if you want for a bare shoulder effect.

## Sleeves

For the size 8 years, CO 32 stitches, knit in fancy stitch by increasing 1st on each side in rows 12, 22, 32 and 42.
Fast off in 60th row. 

For sizes 38-40/42-44/46-48, CO 38/41/44 meshes and increase on each side 1m in rows 12, 22, 32, 42, 52 and 62 (at each 2nd row of the garter block). Fast off after a garter block.

Fold the sleeve, sew it and sew it to the body of the sweater, and make the second sleeve the same.

I hope you will enjoy this sweater and that it will warm you up as much as it will beautify you!  
See you soon for new LCA models!

Hélène


