---
layout: post
lang: fr
title:  "En attendant la neige"
date:   2018-10-13 13:14:00
categories: ['tricot', 'création']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "Un pull tout doux ajouré avec explications gratuites dans toutes les tailles"
asset_path: 'en-attendant-la-neige'
license: 'cc-by-sa'
tags:
  - 'pull'
  - 'tricot'
  - 'doux'
  - 'hygge'
  - 'loose'
  - 'création'
  - 'tutoriel'
  - 'Zeeman'
  - 'neige'
support:
  - jquery
  - gallery
---

Bonjour à toutes et tous!

On continue dans les créations, avec aujourd'hui un modèle ultra simple à réaliser: un pull loose que j'ai nommé "En attendant la neige", tant sa douceur et sa blancheur me font penser à une chaude après-midi au coin du feu à regarder la neige tomber dehors!  
Encore un modèle très très rapide à réaliser aux aiguilles numéro 7, avec 3 pelotes de fil Julia de chez Zeeman pour la taille 8 ans: vous pourrez vous y blottir en moins de 10 heures et pour moins de 10 euros!  
Et il fera plein d'effet pour vos sorties avec son point fantaisie, alternance de point mousse et point rivière ajouré!  
Je vous offre ici les explications pour le réaliser dans toutes les tailles, enfant 8 ans et adulte, sans prise de tête!  
Alors, débutante ou confirmée, lancez-vous!

{% include gallery-layout.html gallery=site.data.galleries.en-attendant-la-neige id_number=1 %}

Vous aurez besoin pour ce pull de:
* Fil Julia de chez Zeeman, 3 pelotes en 8 ans, 6 pelotes environ pour les autres tailles (à confirmer)
* Aiguilles numéro 7

Point fantaisie:  
10 rangs de point mousse (= un "bloc mousse"), 1 rang de point rivière (enrouler 2 fois le fil autour de l'aiguille, lâcher l'un des 2 fils au retour, tuto à retrouver [ici](https://www.garnstudio.com/video.php?id=192&lang=fr)

## Devant et dos

Vous allez tricoter 2 rectangles qui formeront le devant et le dos.
* Taille 38-40: largeur 56cm, hauteur 62 environ
* Taille 42-44: largeur 61cm, même hauteur
* Taille 46-48: largeur 64cm, même hauteur

Ici, pour une taille 8 ans j'ai réalisé 2 rectangles de 40cm de largeur sur 42cm de hauteur.

{% include gallery-layout.html gallery=site.data.galleries.en-attendant-la-neige-carre id_number=2 %}

Montez donc vos mailles (50 en taille 8 ans) (70/76/80 mailles) et tricotez en point fantaisie jusqu'à la hauteur désirée (87 rangs en 8 ans).  
Attention à rabattre après le 10ème rang de point mousse.  
Faites une deuxième pièce identique.  
Cousez les côtés en laissant une large ouverture pour l'emmanchure (environ 16 cm d'ouverture pour l'emmanchure en 8 ans, compter 20 cm pour un 38, 21cm pour un 40-42 et 22cm pour un 44-46).  
Cousez les épaules en laissant une ouverture suffisante pour la tête, un peu plus large si vous le souhaitez pour un effet épaules dénudées.

## Manches

Pour la taille 8 ans, montez 32 mailles, tricoter en point fantaisie en augmentant 1m de chaque côté aux rangs 12, 22, 32 et 42.
Rabattre au 60ème rang. 

Pour les tailles 38-40/42-44/46-48, monter 38/41/44 mailles et augmenter de chaque côté 1m aux rangs 12, 22, 32, 42, 52 et 62 (à chaque 2ème rang du bloc mousse). Arrêtez après un bloc mousse.

Pliez la manche, cousez-la et cousez-la au corps du pull, et réalisez la deuxième manche à l'identique.

J'espère que ce pull vous plaira et qu'il saura vous réchauffer autant que vous embellir!  
A très bientôt pour de nouveaux modèles LCA!

Hélène
