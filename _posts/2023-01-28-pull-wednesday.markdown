---
layout: post
lang: fr
title:  "Pull Wednesday"
date:   2023-01-28 17:00:00
categories: ['tricot', 'creation']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "Un pull au tricot avec des carrés noirs et blancs tiré de la série Wednesday"
asset_path: 'pull-wednesday'
license: 'cc-by-sa'
tags:
  - 'pull'
  - 'tricot'
  - 'sweat'
  - 'création'
  - 'wednesday'
support:
  - jquery
  - gallery
---

Bonjour à tous et toutes!

Je suis de retour sur le site après une période d'absence prolongée.
J'ai regardé comme beaucoup la série **Wednesday** de Tim Burton sur Netflix, et ma fille m'a comme beaucoup demandé de lui faire le pull "carrés" au tricot porté par Wednesday.

J'ai donc créé un modèle de pull (l'un des nombreux que vous pourrez trouver sur le net en ce moment!) que j'ai le plaisir de vous offrir aujourd'hui!

{% include gallery-layout.html gallery=site.data.galleries.pull-wednesday %}

Je le trouve assez proche de l'original, j'en suis assez satisfaite (pour une fois qu'une de mes créations me plaît!)
Alors n'hésitez pas à cliquer sur le pdf ci-dessous, et bon tricot!

{% include download-layout.html pdflink="/assets/pdf/pull-wednesday-lcda.pdf" name="Pull-Wednesday.pdf" %}


