---
layout: post
lang: fr
title:  "Cheshire sweater"
date:   2020-05-01 13:00:00
categories: ['tricot', 'creation']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "Un sweat tout doux aux couleurs du Chat du Cheshire"
asset_path: 'cheshire-sweater'
license: 'cc-by-sa'
tags:
  - 'pull'
  - 'tricot'
  - 'sweat'
  - 'création'
  - 'chat du Cheshire'
  - 'Alice aux pays des merveilles'
  - 'rayures'
support:
  - jquery
  - gallery
---

Bonjour!

Aujourd'hui je vous offre les explications d'un sweat à capuche tout doux aux couleurs du Chat du Cheshire dans Alice au Pays des Merveilles.  
Très simple à faire, réalisé en jersey et côtes, en fil chenille, pour un rendu ultra doux, c'est un pull dans lequel on a envie de cocooner! Il est accessible même aux débutant⋅e⋅s!  
Et aux aiguilles 6,5mm, vous aurez le plaisir d'un tricot rapide et qui monte vite!  
Les explications ci-dessous correspondent à une taille M, pour information le mannequin sur la photo fait une taille 36 et porte habituellement du S.

{% include gallery-layout.html gallery=site.data.galleries.cheshire-sweater %}

Pour le réaliser vous aurez besoin:

* 4 pelotes de fil chenille colori **orchid**
* 4 pelotes de fil chenille colori **lilas**
* 1 pelote de fil chenille colori **rose**
* Aiguille à laine pour les coutures  

Rayures: 6 rangs en colori lilas, 6 rangs en orchid

## Dos et Devant
Monter 60 mailles avec les aiguilles 6 et en colori rose, et tricoter 10 rangs de côtes 1/1 (1 maille endroit, 1 maille envers)  
Continuer aux aiguilles 6,5mm et changer pour le colori lilas et tricoter en rayures (voir ci-dessus) pendant encore 114 rangs (124 rangs au total).  
Rabattre toutes les mailles.  
Tricoter 2 pièces: 1 pour le dos, 1 pour le devant.

## Manches
Monter 48 mailles en colori lilas, et tricoter en rayures (voir ci-dessus) pendant 78 rangs.  
Rabattre toutes les mailles. Tricoter 2 pièces identiques.  

## Capuche
Monter 58 mailles en colori orchid, tricoter pendant 60 rangs en rayures et rabbattre toutes les mailles  

## Poche ventrale
Monter 30 mailles en colori orchid, tricoter 12 rangs en rayures, puis continuer les rayures et rabattre tous les 2 rangs, et à 2 mailles du bord: 9 fois 1 maille de chaque côté. Rabattre les 12 mailles restantes.  

## Finitions
Coudre les épaules sur 16 cm.  
Plier la manche en 2 et positionner le centre de la manche face à la couture de l'épaule. Coudre la manche à l'épaule.  
Coudre les côtés du pull et le dessous des manches.  
Reprendre le bas des manches en colori rose (en relevant les mailles), faire un rang de mailles endroit en tricotant 2 mailles endroit, 2 mailles ensemble pour réduire le poignet. Continuer en côtes 1/1 sur encore 9 rangs. Rabattre toutes les mailles.  
Relever les mailles autour de la capuche avec le colori rose, tricoter en côtes 1/1 pendant 4 rangs. Rabattre.  
Epingler la capuche tout autour du col, en partant du milieu devant. La coudre en grafting.  
Coudre la poche ventrale en faisant correspondre les rayures avec celles du pull.  
Vous pouvez crocheter un rang de mailles serrées sur les bords libres de la poche pour une plus jolie finition si vous savez le faire.  
Rentrez les fils, enfilez votre pull et... enjoy!!
