---
layout: post
lang: fr
title: "Pull de Quidditch Poufsouffle Harry Potter"
date: 2018-11-24 16:00:00
categories: ['tricot', 'création', 'favorite']
main_category: 'sweaters'
comments: true
meta_description: "Un pull de Quidditch Harry Potter, taille adulte, aux couleurs de la maison Poufsouffle"
asset_path: 'pull-quidditch-adulte'
license: 'commercial'
favorite: 80
tags:
  - 'pull'
  - 'création'
  - 'tricot'
  - 'quidditch'
  - 'Zeeman'
  - 'Poufsouffle'
  - 'HarryPotter'
support:
  - jquery
  - gallery
---

Le voici enfin en explications tricot, le pull de Quidditch taille adulte !  
En taille unique (correspond à un S/M) grâce aux point de côtes 2/1 qui lui permettent de s’étendre !

Présenté ici en version Poufsouffle, mais il vous suffira de changer les couleurs pour représenter fièrement votre maison !

{% include gallery-layout.html gallery=site.data.galleries.pull-quidditch-adulte id_number=1 %}

Très facile et rapide à tricoter : aiguilles 4,5mm, pas de point ni de motif compliqué, juste des côtes et 2 couleurs !

Et vous, êtes-vous Gryffondor, Poufsouffle, Serdaigle ou Serpentard ?

Ce pull, conformément au modèle original, est plus long au dos que devant.  
Si vous n’aimez pas cet effet, vous tricotez le bas du dos comme le devant (bande contrastante à la même hauteur que devant) !

Ce modèle est en vente sur [mon espace Ravelry](https://www.ravelry.com/patterns/library/quidditch-sweater-3).

Hélène
