---
layout: post
lang: fr
title: "When In Scotland"
date: 2020-03-28 12:40:00
categories: 'tricot'
main_category: 'shawls'
comments: true
meta_description: "Le célèbre châle de Claire"
asset_path: 'when-in-scotland'
license: 'cc-by-sa'
tags:
  - 'châle'
  - 'tricot'
  - 'Outlander'
  - 'Claire'
  - 'When In Scotland'
support:
  - jquery
  - gallery
---

Bonjour!

En cette période de confinement difficile pour tous, je vous propose aujourd'hui une [traduction]({{ site.url }}/assets/pdf/When-In-Scotland.pdf) du patron de Rilana Riley-Munson, "**When In Scotland**", qui vous permettra de réaliser le châle de Claire dans la série Outlander.

{% include gallery-layout.html gallery=site.data.galleries.when-in-scotland %}

Vous pourrez trouver les explications gratuites en anglais sur la page Ravelry de Rilana en cliquant sur ce [lien](https://www.ravelry.com/patterns/library/when-in-scotland).

Je l'ai réalisé aux aiguilles circulaires (pour pouvoir mettre toutes les mailles) 6 mm, avec 2 brins de Royal de Zeeman, coloris vert et noir.

Un modèle très simple à réaliser pour tricoteuses débutantes et confirmées, mais qui demande quand même une sacrée force en fin d'ouvrage, quand l'ensemble des mailles se trouvent sur le fil.

Je ne donne pas d'échantillon, les rangs pouvant se répéter à l'infini, vous pourrez facilement agrandir ce châle pour l'adapter aux dimensions souhaitées!!

Un très bon tricot à vous et prenez bien soin de vous et vos proches!

LCA

{% include download-layout.html pdflink="/assets/pdf/When-In-Scotland.pdf" name="When-In-Scotland.pdf" %}

