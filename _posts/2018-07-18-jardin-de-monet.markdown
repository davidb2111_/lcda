---
layout: post
lang: fr
title:  "Châle Jardin de Monet"
date:   2018-07-18 19:00:00
categories: ['crochet']
main_category: 'shawls'
comments: true
meta_description: "Un châle dégradé au crochet avec explications gratuites"
asset_path: 'jardin-de-monet'
license: 'cc-by-sa'
tags:
  - 'chale'
  - 'crochet'
  - 'Freia'
  - 'Bluevelvet'
  - 'realisation'
  - 'jardin de monet'
  - 'degrade'
  - 'ete'
support:
  - jquery
  - gallery
---

Bonjour à tous et toutes!

Petite pause vacances pour les créations, mais les aiguilles et le crochet fonctionnent toujours!
Je vous donne rendez-vous à la rentrée pour plein de nouvelles créas, en attendant je vous présente mes réalisations estivales!

Voici aujourd'hui un châle léger pour les soirées d'été, aux couleurs aquatiques que j'adore!

{% include gallery-layout.html gallery=site.data.galleries.jardin-de-monet id_number=1 %}

Réalisé au crochet 3,5mm avec 2 pelotes de la très belle [Merino Fingering Shawl Ball](https://freiafibers.com/blue-velvet.html) de chez Freiafibers.

Vous pouvez retrouver le modèle "Jardin de Monet" de Sylvie Bangert, gratuitement, [ici](https://www.ravelry.com/patterns/library/jardin-de-monet).

A bientôt pour de nouvelles réalisations!
Très bonne journée et un bon crochet!

Hélène

