---
layout: post
lang: fr
title:  "Veste sucre d'orge"
date:   2018-09-19 20:00:00
categories: ['crochet', 'création']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "Une veste au crochet toute douce avec explications gratuites"
asset_path: 'veste-sucre-d-orge'
license: 'cc-by-sa'
tags:
  - 'gilet'
  - 'crochet'
  - 'doux'
  - 'Pasion'
  - 'oversize'
  - 'creation'
  - 'sucredorge'
  - 'automne'
  - 'tutoriel'
  - 'gratuit'
support:
  - jquery
  - gallery
---


Bonjour les tricopines!  

Je vous présente aujourd'hui une grosse veste, que vous pourrez porter en manteau lors des premiers matins frisquets de l'automne!  
Avec un point de crochet qui ressemble aux côtes anglaises du tricot!  
Je me suis largement inspirée, du moins pour ce qui est du point et de la construction verticale, du tuto de Zess que vous pourrez trouver [ici](http://www.zess.fr/tuto-gilet-crochet-a-grosses-mailles-tricot/), et auquel je vous renvoie: ses explications sont très bien faites et il me semble inutile de les reprendre!

J'y apporte plusieurs modifications: sur la construction des manches et les finitions (poignets, bordures bas et devant, col)

J'ai crocheté cette veste en 5 jours, la construction et la grosse taille de crochet la rendent extrêmement rapide à faire!

Pour ce qui est du fil, je l'ai déniché dans un petit magasin caché que j'ai récemment découvert (je défie quiconque de deviner qu'une telle caverne d'Ali Baba se situe derrière cette banale porte d'habitation à la fenêtre cassée!): Fibrex, rue du Crombion à Mouscron, en Belgique. Ce magasin est une merveille, à des prix défiants toute concurrence, dirigé par 2 dames d'une gentillesse exceptionnelle!

Taille 42/46, vous pouvez adapter votre taille en crochetant plus ou moins de rangs sur le dos et les devants.

{% include gallery-layout.html gallery=site.data.galleries.veste-sucre-d-orge %}

Il vous faut:
* 11 pelotes de fil *PASION* de chez Rubi (30% Merino/70% acrylique), soit environ 1320m de fil bulky,
* Crochet n°8.

NB: les mailles en l'air en début de rang ne comptent ici pas pour une demi-bride.

## Dos

Rg1: Crocheter 66 mailles en l'air, et faire 63 demi-brides dans la chaînette obtenue (=64 demi-brides)  
Rg2: 2m en l'air (ne compte pas pour une demi-bride), 64 demi-brides dans le troisième brin qui se trouve derrière la "maille" de chaque demi-bride (voir le tutoriel de Zess)  
Rgs 3 à 28: même chose que le rg 2.

## Devants droit et gauche

Rg1: Crocheter 66 mailles en l'air, et faire 63 demi-brides dans la chaînette obtenue (=64 demi-brides)  
Rg2: 2m en l'air (ne compte pas pour une demi-bride), 64 demi-brides dans le troisième brin qui se trouve derrière la "maille" de chaque demi-bride (voir le tutoriel de Zess)  
Rgs 3 à 10: même chose que le rg 2.

Vous pouvez attacher chaque devant au dos en mailles coulées dès la fin du rang 10, cela évite de couper le fil!  
Laissez une ouverture de 24cm pour les bras.  
Coudre les épaule sur l'envers en mailles coulées.

## Manches

Reprendre les mailles des emmanchures et faire une demi-bride dans chaque maille.  
Continuer en point fantaisie (en piquant dans le troisième brin) en diminuant 1m en début et fin de rang (soit 2 mailles par rang) tous les 5 rangs.  

### Poignets

A 21 rangs de hauteur de manche, réaliser les poignets en mailles serrées:  
Rg1: 1 rg de ms en piquant "normalement" dans les 2 brins de chaque demi-bride, mais en passant 1m toutes les 2 mailles pour resserrer les poignets.  
Rgs 2 à 6: 5 rangs de mailles serrées en piquant dans le brin arrière du rg précédent.

## Bordure du bas

Crocheter des mailles serrées le long de la bordure du bas du gilet, à raison de 1 ms autour de chaque demi-bride (ou 2 mailles en l'air de début de rang) et 1ms dans chaque "côte" (la maille qui ressort et ressemble à la maille tricot).  
Crocheter ensuite 5 rangs de ms en piquant dans le brin arrière. Arrêter le fil.

## Devants et col

Reprendre sur le coin en bas du devant droit de la veste, crocheter 1ms dans chaque demi-bride sur le devant droit, sur le col: 1 ms autour de chaque demi-bride (ou 2 mailles en l'air de début de rang) et 1ms dans chaque "côte" (la maille qui ressort et ressemble à la maille tricot), 1ms dans chaque demi-bride sur le devant gauche.  
Puis, crocheter "1ms, 1 maille en l'air" tout le long du rang (au rang suivant, crocheter 1 maille en l'air au-dessus de la maille serrée, 1ms dans la maille en l'air).  
Arrêter après le 17ème rang.

Il ne reste plus qu'à enfiler votre nouvelle veste qui vous tiendra chaud dès les premiers froids de l'automne.

Bon crochet,

Hélène.
