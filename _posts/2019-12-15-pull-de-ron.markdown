---
layout: post
lang: fr
title: "Pull de Ron"
date: 2019-12-15 10:23:00
categories: ['tricot', 'création']
main_category: ['sweaters', 'free-patterns']
comments: true
meta_description: "Pull de Ron Weasley au tricot tweedé avec lettre jacquard"
asset_path: 'pull-de-ron'
license: 'cc-by-sa'
tags:
  - 'pull'
  - 'création'
  - 'tricot'
  - 'quidditch'
  - 'Cheval Blanc'
  - 'HarryPotter'
  - 'jacquard'
support:
  - jquery
  - gallery
---

Dans la série « mes p’tits pulls de Noël », cela fait quelques années que je voulais réaliser le « vrai » pull de Ron Weasley, celui tricoté par Maman Weasley, avec une laine tweedée « so british » et l’initiale en jacquard... mais remis au goût du jour pour pouvoir être porté par les plus jeunes !

Et c’est chez [Cheval Blanc](https://www.laines-cheval-blanc.com/fr/laine-fil-tricot-hiver/15-country-tweed.html#/country_tweed-bordeaux_153) que j’ai (enfin) trouvé le fil de mes rêves pour réaliser mon projet !  
Je l’ai tricoté en Country Tweed, un fil tweedé bordeaux qui rappelle parfaitement la couleur de l’original!  
J’ai réalisé ici un pull près du corps, pas trop long, aux emmanchures droites.  
Je vous le présente en taille S et je vous offre ici les explications jusqu’à la taille XXL !  
Vous pouvez le personnaliser en brodant l’initiale de votre choix, ou pourquoi pas laisser le « R » de Ron si vous les souhaitez.  

{% include gallery-layout.html gallery=site.data.galleries.pull-de-ron id_number=1 %}

## Matériel

* Fil Country Tweed de Cheval Blanc : 8/9/9/10/10 pelotes coloris 153 ([Bordeaux](https://www.laines-cheval-blanc.com/fr/laine-fil-tricot-hiver/15-country-tweed.html#/country_tweed-bordeaux_153)) et 1 pelote coloris 038 ([Mastic](https://www.laines-cheval-blanc.com/fr/laine-fil-tricot-hiver/15-country-tweed.html#/country_tweed-mastic_038)) pour la lettre
* Aiguilles circulaires 4 et 4,5mm
* Aiguille à laine pour les coutures et la broderie

Echantillon : 10cm x 10cm= 19m x 28rgs

## Explications

**Dos**

Monter 82/90/98/104/116 mailles avec les aiguilles 4.  
Tricoter 10 rangs de côtes 2/1.  
Continuer en jersey aux aiguilles 4,5mm.  
A 152/158/164/170/176 rangs de hauteur totale, rabattre les 30/32/34/36/38 mailles centrales.  
En même temps, rabattre pour les épaules 1x7m puis 2x8m / 1x8m puis 2x9m / 1X9m puis 2x10m / 2x10m puis 1x11m / 3x12m  

**Devant**

Tricoter comme au dos.  
A 140/146/152/158/164 rgs de hauteur totale, rabattre pour l’encolure :  
* 18m, puis 1X3m, 2x2m, 2x1m
* 20m, puis 1X3m, 2x2m, 2x1m
* 20m, puis 1x4m, 2x2m, 2x1m
* 22m, puis 1x4m, 2x2m, 2x1m
* 22m, puis 1x4m, 1x3m, 1x2m, 2x1m

Rabattre les épaules comme au dos.

**Manches (x2)**

Monter 47/49/51/53/55 mailles en bordeaux avec les aiguilles 4. Tricoter 10 rangs en côtes 2/1.  
Continuer en jersey endroit aux aiguilles 4,5mm en augmentant de chaque côté :  
* 8x1m tous les 15 rangs
* 8x1m tous les 15 rangs
* 2x1m tous les 15 rangs, 7x1m tous les 13 rangs
* 7x1m tous les 13 rangs, 3x1m tous les 11 rangs 
* 6x1m tous les 12 rangs, 7x1m tous les 8

A 144 rangs de hauteur totale, rabattre toutes les mailles.

**Finitions**

Coudre les épaules.  
Plier la manche en 2 et épingler le milieu de la manche face à la couture de l’épaule.  
Coudre la manche à l’emmanchure.  
Coudre les côtés du pull et le dessous des manches en une seule fois.  

**Col**

Avec un fil rouge et une aiguille circulaire n°4, relever les mailles tout autour de l’encolure.  
Tricoter 5 rangs en côtes 2/1, puis 4 rangs en jersey endroit pour obtenir un effet roulotté.  
Rabattre toutes les mailles.  

**Broderie**

Avec le fil couleur mastic, broder au point de mailles l’initiale, au milieu de la partie supérieure du pull.  
Si vous souhaitez réaliser la lettre de votre choix, elle fait approximativement 40 mailles en hauteur sur 36 mailles en largeur.  
Des grilles de lettres existent en recherchant « grille lettre point de croix ».  
Je vous [fournis]({{ site.url }}/assets/pdf/grille-a.pdf) celle que j’ai réalisée moi-même pour le « A ».

{% include download-layout.html pdflink="/assets/pdf/grille-a.pdf" name="grille-a.pdf" %}
